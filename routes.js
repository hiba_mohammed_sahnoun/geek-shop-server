//declaration des Routes
const router = require('express').Router();

//const productModel = require('./models/products');
//1111ou const {getProducts} = require('./models/products');
//const voucherModel = require('./models/vouchers');

const { getProducts } = require('./models/products');
const { getVouchers ,setVoucher,patchVoucher,deleteVoucher} = require('./models/vouchers');
const {logRouteCall,checkVoucherParam} = require("./middlewares");


router.get('/', (req, res) => {
    res.send('Bonjour à tous');
});

//création de la route pour products bdd en dur
//router.get('/products', (req, res) => {
   // const products = productModel.getProducts();
//111 et ici const products = getProducts;
   // res.json(products);
//});

//création de la route pour bdd sql

router.get('/products', (req, res) => {
   
   getProducts()
   
   .then(products=>{
    
       res.json(products[0]);
       
   })
   .catch(err=>{
       console.log(err.message);
   })
   
});

//Requeter affichage bdd
//logRouteCall est mon middleware il passe apres la route et avant (req,res)
router.get('/vouchers',logRouteCall, (req, res) => {
    // const vouchers    = voucherModel.getVouchers();
    // res.json(vouchers);
 
    //console.log("test result")
    getVouchers()
    
    .then(vouchers=>{
     
        res.json(vouchers[0]);
        
    })
    .catch(err=>{
        console.log(err.message);
    })
    
 });

 //Insertion BDD
 router.post('/vouchers',checkVoucherParam, (req, res) => {
    //Recuperer les valeurs du coupons dans le body de la req
    
        const vouchers = {name:req.body.name  , reduction:parseFloat(req.body.reduction) };
        
        console.log(vouchers);

        setVoucher(vouchers.name,vouchers.reduction)
        .then(result=>res.send(result));
        console.log("test route apres middleware")
  
        
    
  });


  //MAJ Bdd

  router.patch('/vouchers',(req, res) =>{
  if(req.body && req.body.name && req.body.reduction ){
    const vouchers = {name:req.body.name  , reduction:parseFloat(req.body.reduction)}
    patchVoucher(vouchers) ;
  }
 res.send(); 
})

  router.put('/vouchers', (req, res,next) => {
    //Recuperer les valeurs du coupons dans le body de la req
   
        if(req.body && req.body.name && req.body.reduction ){
            const vouchers = {name:req.body.name  , reduction:parseFloat(req.body.reduction) };
            getVouchers(vouchers)
          
            .then(vouchers=>{
                if(vouchers[0].length){
                    patchVouchers(req.body);
                }else{
                    setVouchers(req.body);
                }
                res.send();
            })
            .catch((error)=>{
                res.send(error.message)
            })            
            
        }
        
  })

  router.delete('/vouchers',(req, res,next)=>{
    if(req.body && req.body.name && req.body.reduction ){
      console.log(req.body)
        deleteVoucher(req.body) ;
    }
    res.send(); 
  });

  


module.exports = router;