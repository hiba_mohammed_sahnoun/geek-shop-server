// My middleware

 function logRouteCall(req, res, next) {
    //log route calls
    console.log(`route call :${req.originalUrl}`);

    //sort du middleware pour aller a la route
    next();
}

function checkVoucherParam(req, res, next) {
    if(req.body && req.body.name && req.body.reduction){
        console.log(`checkVoucherParam`);
        next();
    }
    else{
        res.send("il manque le name ou la value");

    }
}

module.exports={
    logRouteCall,
    checkVoucherParam,
}