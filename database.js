//connecter la database
// get the client
const mysql = require('mysql2');

const DBConnector = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
  });

  // doc dotenv//------------------------------------------------
//const db = require('db');
//
//db.connect({
//  host: process.env.DB_HOST,
//  username: process.env.DB_USER,
//  password: process.env.DB_PASS
//})
//------------------------------------------------

module.exports = DBConnector;