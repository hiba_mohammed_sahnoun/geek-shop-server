
//toute les données datajson 
//const data = require('../data');
//data.products; //ici je demande products dans datajson 

//ici je choisi products dans les données datajson 
//const {products} = require('../data.json');

const DBConnector = require('../database.js')

//ici avant connexion bdd
//const getProducts =() =>{
//    return products
//}

const getProducts=()=> {
    return DBConnector.promise().query('SELECT code , description , price FROM products')
}



module.exports = {
    getProducts,
    
}

