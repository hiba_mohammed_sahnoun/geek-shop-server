//const {vouchers} = require('../data.json');
const DBConnector = require('../database.js')


const getVouchers =() =>{
    return DBConnector.promise().query('SELECT name , reduction  FROM vouchers')
}




const setVoucher =(name,reduction) =>{
    return DBConnector.promise().query("INSERT INTO vouchers (name , reduction) VALUES (?,?) ",[name,reduction])

   
}

const patchVoucher = (vouchers) =>{ 
    return DBConnector.promise().query("UPDATE vouchers SET reduction = ? WHERE name = ? ", [vouchers.reduction,vouchers.name]);
}
//const upDateVouchers =(name,reduction) =>{
//    return DBConnector.promise().query(
//        `UPDATE vouchers SET 
//        (name=? , reduction =? )`,[name,reduction])  
//}

const deleteVoucher =(vouchers) =>{
    return DBConnector.promise().query(
        `DELETE FROM vouchers WHERE 
        name = ? `,vouchers.name)

   
}

module.exports = {
    getVouchers,
    setVoucher,
    patchVoucher,
    deleteVoucher
    //upDateVouchers,
}